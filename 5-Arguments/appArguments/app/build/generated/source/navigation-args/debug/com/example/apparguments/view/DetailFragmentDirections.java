package com.example.apparguments.view;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.example.apparguments.R;

public class DetailFragmentDirections {
  private DetailFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionDetailFragmentToListFragment() {
    return new ActionOnlyNavDirections(R.id.action_detailFragment_to_listFragment);
  }
}
