# Android JetPack

Desarrollo de Android JetPack

## Navigation.
0. No olvidar importar las dependecias  en build app:

```sh
plugins {
    id 'com.android.application'
    id 'kotlin-android'
    id 'androidx.navigation.safeargs' //Linea Agregada
}
android {
    ... //Lineas agregadas:
    buildFeatures {
            dataBinding true
        }
        ...
}
//Linea Agregadas:
def archLifecycleExtensionsVersion = '1.1.1'
def supportDesignVersion = '28.0.0'
def MaterialDesignVersion = '1.2.1'
def retrofitVersion ='2.9.0'
def roomVersion = '2.2.5'
def NavVersion = '2.3.1'
def rxJavaVersion = '2.1.1'
def preferencesVersion = '1.1.1'
def glideVersion = '4.11.0'

dependencies {

    implementation "org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version"
    implementation 'androidx.core:core-ktx:1.3.2'
    implementation 'androidx.appcompat:appcompat:1.2.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.2'

    implementation "com.google.android.material:material:$MaterialDesignVersion"
    implementation "com.android.support:design:$supportDesignVersion"
    implementation "com.android.support:palette-v7:$supportDesignVersion"
//Lineas Agregadas:
    implementation "android.arch.lifecycle:extensions:$archLifecycleExtensionsVersion"
    implementation "androidx.room:room-runtime:$roomVersion"
    implementation "androidx.room:room-compiler:$roomVersion"
    implementation "androidx.room:room-ktx:$roomVersion"
    implementation "androidx.navigation:navigation-fragment-ktx:$NavVersion"
    implementation "androidx.navigation:navigation-ui-ktx:$NavVersion"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.1.1"
    implementation "com.squareup.retrofit2:retrofit:$retrofitVersion"
    implementation "com.squareup.retrofit2:converter-gson:$retrofitVersion"
    implementation "com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion"
    implementation "io.reactivex.rxjava2:rxjava:$rxJavaVersion"
    implementation "io.reactivex.rxjava2:rxandroid:$rxJavaVersion"
    implementation "com.github.bumptech.glide:glide:$glideVersion"
    implementation "androidx.preference:preference:$preferencesVersion"
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'


    testImplementation 'junit:junit:4.+'
    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'
}
```

y en build project: 

```sh
dependencies {
        classpath "com.android.tools.build:gradle:4.1.0"
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"

        // Las 2 siguientes lineas fueron agregadas:
        def nav_version = "2.3.1"
        classpath "androidx.navigation:navigation-safe-args-gradle-plugin:$nav_version"

    }
```

1. Crear los paquetes: 


| Paquete | Descripcion |
| ------ | ------ |
| Model | Contiene las clases |
| View | Contiene las vistas |
| ViewModel | Contiene el viewModel|
 
2. Dentro de la vista crear 2 Fragment en Blanco.
3. En los XML de los Fragment Cambiar la etiqueta a layout

![](cambiarel_XML_a-layout.png)

4. Crear el NavHostFragment en el activity main:

![](add_NavHostFragment.png)

5. Crear el Navigation Component; new>AndroidResourceFile:

![](add_newResource_Navigation_file.png)


5. Jalar los Fragment al navigation Component:

![](navigation_component.png)

6. Se crea el onClick Listener:

    ### Desde OnCreadtedView:

![](setOnClickListener_desde_onCreateview.png)

### o Desde onViewCreated:

![](setOnClickListener_desde_onViewCreated.png)

7. Crear el UpBackButton:

![](UpBackButton.png)